//
// Created by crypt on 06/11/18.
//
#include <deque>
#include <thread>
#include <iostream>
#include <thread>
using namespace std;

class TOH {

 private:
  uint32_t loop;
  deque<uint32_t> first;
  deque<uint32_t> second;
  deque<uint32_t> third;
  uint32_t counter = 0;
  bool debug;

  // uint32_t depth;
 private:
  void wait() { if (debug) {
//    this_thread::sleep_for(chrono::milliseconds(250));
  }}

  void print(string action, uint32_t depth) {
    //auto tallest = first.size() > second.size() ? first.size() : second.size();
    //tallest = tallest > third.size() ? tallest : third.size();
    cout << counter++ << " ------------- " << action << ":" << depth << endl;
    for (int i = 9; i >= 0; i--) {
      //auto i = 9 - t;
      cout << " | " << get(first, i) << " | " << get(second, i) << " | " << get(third, i) << " |" << endl;
    }
    wait();
  }

  string get(deque<uint32_t> &v, int index) {
    auto i = v.size() - index;
    return i < v.size() ? to_string(v[i]) : " ";
  }

  void move(deque<uint32_t> &source, deque<uint32_t> &target, uint32_t depth) {
    auto tmp = source.front();
    source.pop_front();
    target.push_front(tmp);
    print("move", depth);
  }

  uint32_t getValue(deque<uint32_t> &q) { return q.empty() ? UINT32_MAX : q.front(); }

  void shift(deque<uint32_t> &s, deque<uint32_t> &aux, deque<uint32_t> &t, uint32_t depth) {

    cout << "shift " << depth << endl;
    if (depth > 0) {
      shift(s, t, aux, depth - 1);
      move(s, t, depth);
      shift(aux, s, t, depth - 1);
    }
  }

  void somethingelse(deque<uint32_t> &s, deque<uint32_t> &aux, deque<uint32_t> &t, uint32_t depth) {
  //  this->depth++;
    while (depth != 0 && this->loop > 0) {
      depth --;
      this->loop--;


      if (! shift_source(s, aux, t) ) {
        bool aux_less_t = getValue(aux) < getValue(t);
        bool t_less_s = getValue(t) < getValue(s);

        if (aux_less_t) {
          if (t_less_s) {
            move(aux, t, 0);
            // shift(aux, s, t, 0);
          } else {
            // s_less_t
            move(aux, s, 0);
            // shift(aux, s, t, 0);
          }
        } else if (getValue(t) < getValue(s)) {

          shift(t, s, aux, 0);
        }
      }
    }
    // this->depth--;
  }

  bool shift_source(deque<uint32_t> &s, deque<uint32_t> &aux, deque<uint32_t> &t) {
    if (s.front() < getValue(t)) {
      move(s, t, 0);
      shift(aux, s, t, 0);
    } else if (s.front() < getValue(aux)) {
      move(s, aux, 0);
      shift(t, s, aux, 0);
    } else {
      return false;
    }
    return true;
  }

 public:
  TOH(uint32_t elements, bool debug) : debug(debug) {
    // depth = -1;
    if (debug) {
      this->loop = 10;
    } else {
      loop = UINT32_MAX;
    }
    for (int i = 0; i < elements; ++i) {
      first.emplace_back(i + 1);
    }
  }

  void shift() {
    if (!this->first.empty()) {
      print("start", first.size());
      this->shift(first, second, third, first.size());
      print("end", first.size());
    }
  }
};


//
//void wait();
//void toh_shuffle(deque<uint32_t> &source, deque<uint32_t> &target, deque<uint32_t> &aux) {
//  while (!source.empty()) {
//
//    if (source.front() < target.front()) {
//      move(source, target);
//    } else if (source.front() < aux.front()) {
//      move(source, aux);
//    }
//
//    print("shuffle");
//
//  }
//}

//uint32_t loop = 15;
//
//void toh(deque<uint32_t> &f, deque<uint32_t> &s, deque<uint32_t> &t) {
//  auto f_size = f.size();
//  auto i = 15;
//  deque<uint32_t> &source = f;
//  deque<uint32_t> &target = t;
//  deque<uint32_t> &aux = s;
//
//  while ((!f.empty() || !s.empty()) && loop > 0 && (t.size() < f_size)) {
//    loop--;
//    if (t.empty() || s.empty()) {
//      print("pop-first");
//
//      if (t.empty()) {
//        // && !s.empty() && f.front() < s.front()) {
//        move(f, t);
//        toh_shuffle();
//        aux = f;
//        target = t;
//        source = s;
//      } else {
//        move(f, s);
//        aux = f;
//        target = s;
//        source = t;
//      }
//    } else {
//      toh_shuffle(source, target, aux);
//    }
//    wait();
//
//  }
//}
/*
    else if (s.empty()) {
      if (!t.empty() && f.front() < t.front()) {
        move(f, t);
      } else {
        move(f, s);
      }
    } else if (t.front() < s.front()) {
      if (t.front() < f.front()) {
        move(t, f);
      } else {
        move(t, s);
      }
    } else
      // t > s
    if (s.front() < f.front()) {
      // f > t > s
      move(s, f);
    } else {
      move(s, t);
    }
    else
    // f > s > t
    if (t.front() < s.front()) {
      move(t, s);
    } else
      // f > t > s
    if (s.front() < t.front()) {
      move(s, t);
    } else {
      move(s, f);
    }
*/




int main(int argc, char **argv) {
  TOH toh(7, true);
  toh.shift();
}
