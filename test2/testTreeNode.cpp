//
// Created by crypt on 26/10/18.
//

#include "gtest/gtest.h"
#include "../src/TreeNode.hpp"

TreeNode<int> *createFullBinaryTreeSize7() {
  auto root = new TreeNode<int>(4);
  root->value = 4;
  root->left = new TreeNode<int>(2);
  root->left->left = new TreeNode<int>(1);
  root->left->right = new TreeNode<int>(3);

  root->right = new TreeNode<int>(6);
  root->right->left = new TreeNode<int>(5);
  root->right->right = new TreeNode<int>(7);
  return root;
}

template <typename T>
auto makeComparator(T previous) {
  return [&previous](TreeNode<T> *node) -> Progress {
    T tmp = previous;
    if (previous < node->value) {
      previous = node->value;
      std::cout << tmp << " < " << node->value << std::endl;
      return CONTINUE;
    } else {
      std::cout << "ERROR " << tmp << " < " << node->value << std::endl;;
      return DONE;
    }
  };
}
TEST(testInOrder, inOrderWorks) {
  auto root = createFullBinaryTreeSize7();

  auto visitor = [](TreeNode<int> *node) -> Progress {
    std::cout << node->value << std::endl;
    return CONTINUE;
  };
  std::cout << std::endl;
  root->inOrderVisitor(visitor);

}







TEST(testInOrder, comparator_state) {
  auto root = createFullBinaryTreeSize7();

  std::cout << std::endl;
  //auto comparator = makeComparator<int>(INT32_MIN);
  //root->inOrderVisitor(comparator);
}

TEST(testInOrder, comparator_will_fail)
{
  auto root = createFullBinaryTreeSize7();

  std::cout << std::endl << root->left->right->value << "->" << 9 << std::endl;
  //Progress (comparator)(TreeNode<int> *);
  auto comparator = makeComparator<int>(INT32_MIN);
  // root->inOrderVisitor(comparator);
  //fn = (makeComparator<int>(INT32_MIN));
  //root->inOrderVisitor(makeComparator<int>(INT32_MIN));
}
