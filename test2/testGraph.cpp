//
// Created by crypt on 09/10/18.
//
#include "gtest/gtest.h"
#include "../src/Graph.hpp"

TEST(Graph_addNode, empty_name) {
  Graph graph;
  stringstream testStream;
  char line[256];

  graph.streamGraphvizOutput(testStream);
  testStream.getline(line, 256);
  EXPECT_STREQ(line, "digraph {");
  testStream.getline(line, 256);
  EXPECT_STREQ(line, "}");
}

TEST(Graph_addNode, simple_name) {
  Graph graph("any_name");
  stringstream testStream;
  char line[256];

  graph.streamGraphvizOutput(testStream);
  testStream.getline(line, 256);
  EXPECT_STREQ(line, "digraph any_name {");

  testStream.getline(line, 256);
  EXPECT_STREQ(line, "}");
}

TEST(Graph_addNode, simple_name_with_one_node) {
  Graph graph("any_name");
  graph.addNode("any_node");
  stringstream testStream;
  char line[256];

  graph.streamGraphvizOutput(testStream);
  testStream.getline(line, 256);
  EXPECT_STREQ(line, "digraph any_name {");

  testStream.getline(line, 256);
  EXPECT_STREQ(line, "any_node;");

  testStream.getline(line, 256);
  EXPECT_STREQ(line, "}");
}

TEST(Graph_addNode, simple_name_with_two_nodes) {
  Graph graph("any_name");
  graph.addNode("any_node");
  graph.addNode("second_node");
  stringstream testStream;
  char line[256];

  graph.streamGraphvizOutput(testStream);
  testStream.getline(line, 256);
  EXPECT_STREQ(line, "digraph any_name {");

  testStream.getline(line, 256);
  EXPECT_STREQ(line, "any_node;");
  testStream.getline(line, 256);
  EXPECT_STREQ(line, "second_node;");
  testStream.getline(line, 256);
  EXPECT_STREQ(line, "}");
}

TEST(Graph_addNode, one_edge_between_two_nodes) {
  Graph graph("any_name");
  auto n1 = graph.addNode("n1");
  auto n2 = graph.addNode("n2");
  graph.addEdge(n1, n2, "edge");
  stringstream testStream;

  char line[256];
  graph.streamGraphvizOutput(testStream);

  testStream.getline(line, 256);
  EXPECT_STREQ(line, "digraph any_name {");

  testStream.getline(line, 256);
  EXPECT_STREQ(line, "n1;");

  testStream.getline(line, 256);
  EXPECT_STREQ(line, "n2;");

  testStream.getline(line, 256);
  EXPECT_STREQ(line, "n1 -> n2[label=\"edge\"];");
}
