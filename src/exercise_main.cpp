

#include <string>
#include <thread>
#include <iostream>
#include <cassert>
using namespace std::this_thread;
using namespace std;
inline std::string trim(std::string &str) {
  str.erase(0, str.find_first_not_of(' '));       //prefixing spaces
  str.erase(str.find_last_not_of(' ') + 1);         //surfixing spaces
  return str;
}

int calculate_sum_of_digit() {

  string input;
  do {
    cout << "Insert your number: ";
    cin >> input;
    trim(input);
    if (!input.empty()) {
      try {
        long number = stol(input);
        auto result = 0u;
        if (number > 0) {
          while (number > 0) {
            result += number % 10;
            number /= 10;
          }
          cout << input << " -> " << result << endl;
        } else {
          cout << "Not a positive number: '" << input << "'" << endl;
        }
      } catch (std::invalid_argument &e) {
        cerr << "ERROR: Not a number '" << input << "'" << endl << flush;
        sleep_for(100ns);
      }
    }
  } while (!input.empty());
  return 0;
}





// --
// -- Fraction
// --
class fraction {
 public:
  fraction(int num, int denum) : denum(denum), num(num) {}
  fraction operator+(const fraction &other) {
    return fraction(this->num * other.denum + this->denum * other.num,
        this->denum * other.denum);
  }
  fraction operator*(const fraction &other) {
    return fraction(this->num * other.num, this->denum * other.denum);
  }
  fraction operator/(const fraction &other) {
    return fraction(this->num * other.denum, this->denum * other.num);
  }
  int num;
  int denum;

};

fraction fraction_calculator_sum(int a, int b, int c, int d) {
  return fraction(a, b) + fraction(c, d);
}

fraction fraction_calculator_mul(int a, int b, int c, int d) {
  return fraction(a, b) * fraction(c, d);
}

fraction fraction_calculator_div(int a, int b, int c, int d) {
  return fraction(a, b) / fraction(c, d);
}

int main(int argc, char **argv) {
  fraction v1 = fraction_calculator_sum(1, 2, 1, 3);
  assert(v1.num == 5 && v1.denum == 6);
  fraction v2 = fraction_calculator_sum(1, 2, 0, 1);
  assert(v2.num == 1 && v2.denum == 2);

  fraction v3 = fraction_calculator_mul(1, 2, 1, 3);
  assert(v3.num == 1 && v3.denum == 6);
  fraction v4 = fraction_calculator_mul(1, 2, 0, 1);
  assert(v4.num == 0);

  {
    fraction v = fraction_calculator_div(1, 2, 1, 3);
    assert(v.num == 3 && v.denum == 2);
  }
  {
    fraction v = fraction_calculator_div(2, 1, 1, 2);
    assert(v.num == 4 && v.denum == 1);
  }


  {
    // fraction v = fraction_calculator_div(1, 2, 0, 1);
    //assert(v4.num == 0 );
  }

  {
    fraction v = fraction(1,2) + fraction(1,2) * fraction(1,2) / fraction(1,3);
    assert( v.num == 3 && v.denum == 2);
  }
}
