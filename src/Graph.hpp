//
// Created by crypt on 09/10/18.
//

#ifndef GRAPH_GRAPH_HPP
#define GRAPH_GRAPH_HPP
#include <string>
#include <vector>

using namespace std;

class Graph;
class Node;
class Edge;

class Graph {
 private:
  string label;
  vector<Node *> nodes;
  vector<Edge *> edges;
 public:
  const Node* addNode(string name);

  void addEdge(const Node *source, const Node *target, string label = "", string color = "black", float width = 1.0f);

  void streamGraphvizOutput(ostream &stream) const;
  explicit Graph(const string &name = "");
};
class Node {
 public:
  explicit Node(const string &label);
 private:
  string label;
 public:

  friend class Graph;
};

class Edge {
 public:
  Edge(const Node *source, const Node *target, string label, string color, float width);
 private:
  const Node *source;
  const Node *target;
  string label;
  string color;
  float width;
 public:
  friend class Graph;
};

#endif //GRAPH_GRAPH_HPP
