//
// Created by crypt on 26/10/18.
//

#ifndef GRAPH_TREENODE_HPP
#define GRAPH_TREENODE_HPP
#include <functional>

enum Progress {

  CONTINUE,
  DONE,
};

template<typename T>
class TreeNode {

 public:
  TreeNode<T> *left;
  TreeNode<T> *right;
  T value;
  TreeNode(T value) : value(value) {}
  bool isLeaf() {
    return this->left == nullptr && this->right == nullptr;
  };

  Progress inOrderVisitor(std::function<Progress(TreeNode<T> *)> visitor) {
    if (this->left != nullptr && this->left->inOrderVisitor(visitor) == DONE) {
      return DONE;
    }
    if (visitor(this) == DONE) {
      return DONE;
    }
    if (this->right != nullptr && this->right->inOrderVisitor(visitor) == DONE) {
      return DONE;
    }
    return CONTINUE;
  };

  template<typename F>
  Progress inOrderVisitor(F visitor) {
    visitor(this);
    return CONTINUE;
  }
};
#endif //GRAPH_TREENODE_HPP
