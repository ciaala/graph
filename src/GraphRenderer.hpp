//
// Created by crypt on 23/10/18.
//

#ifndef GRAPH_GRAPHRENDERER_HPP
#define GRAPH_GRAPHRENDERER_HPP
#include <string>
#include "Graph.hpp"
using namespace std;
class GraphRenderer {
 private:
  string filename;
 public:
  explicit GraphRenderer(string filename);

  void write(const Graph &graph);
};

#endif //GRAPH_GRAPHRENDERER_HPP
