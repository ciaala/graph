//
// Created by crypt on 23/10/18.
//

#include <sstream>
#include <cgraph.h>
#include <types.h>
#include <gvc.h>
#include <iostream>
#include "GraphRenderer.hpp"
using namespace std;
GraphRenderer::GraphRenderer(string filename) : filename(filename) {
}

void GraphRenderer::write(const Graph &graph) {

  stringstream stream;
  graph.streamGraphvizOutput(stream);
  //const auto * string = stream.str().c_str();
  cout << stream.str() << endl << flush;
  GVC_t *gvc = gvContext();
  graph_t *g = agmemread(stream.str().c_str());
  gvLayout(gvc, g, "dot");
  FILE *fp = fopen(filename.c_str(), "w+");
  gvRender(gvc, g, "svg", fp);

  gvFreeLayout(gvc, g);

  agclose(g);
  if ( gvFreeContext(gvc) != 0) {
    std::cerr << "cannot cleanup graph context" <<  std::endl;
  }

  fclose(fp);
}

