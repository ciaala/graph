//
// Created by crypt on 23/10/18.
//


#include "Graph.hpp"
#include "GraphRenderer.hpp"
int main(int argc, char **argv) {
  Graph graph("First_Graph");
  const auto n0 = graph.addNode("N0");
  const auto n1 = graph.addNode("N1");
  const auto n2 = graph.addNode("N2");
  const auto n3 = graph.addNode("Vika");
  const auto n4 = graph.addNode("Fika");
  graph.addEdge(n0, n1);
  graph.addEdge(n0, n2);
  graph.addEdge(n2, n0);
  graph.addEdge(n1, n0);
  graph.addEdge(n3, n4,"lecca");
  GraphRenderer renderer("/tmp/example.svg");
  renderer.write(graph);
  return 0;
}
