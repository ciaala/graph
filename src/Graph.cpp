//
// Created by crypt on 09/10/18.
//

#include "Graph.hpp"
#include <iostream>
void Graph::streamGraphvizOutput(ostream &stream) const {
  stream << "digraph " << this->label << ((this->label.size() > 0) ? " {" : "{") << endl;
  for (auto *node : this->nodes) {
    stream << node->label << ";" << endl;
  }
  for (auto *edge: this->edges) {
    stream << edge->source->label << " -> " << edge->target->label << "[label=\"" << edge->label << "\"];" << endl;
  }
  stream << "}";
}
Graph::Graph(const string &name) : label(name) {
}
const Node *Graph::addNode(string name) {
  Node *node = new Node(name);
  nodes.emplace_back(node);
  return node;
}
void Graph::addEdge(const Node *source,
                    const Node *target,
                    string label,
                    string color,
                    float width) {
  edges.emplace_back(new Edge(source, target, label, color, width));
}
Edge::Edge(const Node *source,
           const Node *target,
           string label,
           string color,
           float width) :
    source(source), target(target), label(label), color(color), width(width) {

}

Node::Node(const string &label) : label(label) {}


